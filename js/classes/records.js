var Records = {
	salvar: function(nomeJogador, pontuacao) {
		records = JSON.parse(window.localStorage.getItem('Records'));

		//Se nunca foi salvo nenhum record, então cria o array de records
		if (!records)
			records = [];

		records.push(Record.new(nomeJogador,pontuacao));

		//Ordena o array de records de acordo com a pontuacao
		records.sort(compare);

		//Remove o último score(caso possua mais de 10 records)
		if (records.length > 10) {
			records = records.slice(0,-1);
		}

		//Salva o array de records
		window.localStorage.setItem('Records',JSON.stringify(records));
	}
}

var Record = {
	new: function (nome, pontuacao) {
		return {
			nome: nome,
			pontuacao: pontuacao
		}
	}
}

//Método para 
function compare(a, b) 
{
	if (a.pontuacao < b.pontuacao ) 
		return 1;

	if (a.pontuacao > b.pontuacao ) 
		return -1;

	return 0;
}