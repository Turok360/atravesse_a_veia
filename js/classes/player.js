
//Classe do jogador
var Player = enchant.Class.create(enchant.Sprite, {
    initialize: function () {
        var jogador = imagens.jogador;

		//Define a largura e altura da imagem
        enchant.Sprite.call(this, jogador.largura, jogador.altura);

        //Carrega nome do jogador do form da index.
        this.nome = $('#namePlayer').val();

        //Associa a imagem
        this.image = gameGeneral.assets[jogador.arquivo];

        //Indica que o jogador irá começar no frame 1
        this.frame = 1;

        //Variavel de uso interno para usar o frameset
        this.position = side.down;

        //Indica se o jogador deve movimentar 
        this.move = false;

        //Posicao X e Y do jogador na tela (inicial)
        this.x = 60;
        this.y = 0;

       	//Score inicial do jogador
       	this.score = 0;
       	
       	//Escala da imagem
       	this.scale(jogador.escala, jogador.escala);
    },
    resetPosition: function () {
        //Posicao X e Y do jogador na tela (inicial)
        this.x = 60;
        this.y = 0;
    },
    onenterframe: function(e) {
		/* MOVIMENTAÇÃO DO JOGADOR */
		var player = this;

		//Move o player somente para um lado
		//pois não tem sprite para andar horizontalmente

		//Move para a esquerda
		if (
			gameGeneral.input.left &&
			!gameGeneral.input.right &&
			!gameGeneral.input.up &&
			!gameGeneral.input.down 
			) {
			//Verifica se o jogador está se movendo e se está na posição correta
			//Pois, se não fizer esta validação o movimento pode não
			//funcionar corretamente
			if (!player.move || player.position != side.Left) {
				//Seto o lado que o jogador esta movimentando
				player.position = side.Left;
				//Ativa os frames de movimentacao
				player.frame = frameSet.Left.frames;
				//Indica que o jogador está se movendo
				player.move = true;
			}


			if (player.x - tamanhoMovimento < 0) {
				player.x = 0;
			} else {
				player.x -= tamanhoMovimento;
			}
		} else if (
			!gameGeneral.input.left &&
			gameGeneral.input.right &&
			!gameGeneral.input.up &&
			!gameGeneral.input.down
			) {
			//Verifica se o jogador está se movendo e se está na posição correta
			//Pois, se não fizer esta validação o movimento pode não
			//funcionar corretamente
			if (!player.move || player.position != side.Right) {
				//Seto o lado que o jogador esta movimentando
				player.position = side.Right;
				//Ativa os frames de movimentacao
				player.frame = frameSet.Right.frames;
				//Indica que o jogador está se movendo
				player.move = true;
			}

			if (player.x + player.width + tamanhoMovimento > gameGeneral.width) {
				player.x = gameGeneral.width - player.width;
			} else {
				player.x += tamanhoMovimento;
			}
		} else if (
			!gameGeneral.input.left &&
			!gameGeneral.input.right &&
			gameGeneral.input.up &&
			!gameGeneral.input.down 
			) {
			//Verifica se o jogador está se movendo e se está na posição correta
			//Pois, se não fizer esta validação o movimento pode não
			//funcionar corretamente
			if (!player.move || player.position != side.Up) {
				//Seto o lado que o jogador esta movimentando
				player.position = side.Up;
				//Ativa os frames de movimentacao
				player.frame = frameSet.Up.frames
				//Indica que o jogador está se movendo
				player.move = true;
			}
			//Move para cima
			if (player.y - tamanhoMovimento < 0) {
				player.y = 0;
			} else {
				player.y -= tamanhoMovimento;
			}
			player.y -= tamanhoMovimento;
		} else if (
			!gameGeneral.input.left &&
			!gameGeneral.input.right &&
			!gameGeneral.input.up &&
			gameGeneral.input.down 
			) {
			//Verifica se o jogador está se movendo e se está na posição correta
			//Pois, se não fizer esta validação o movimento pode não
			//funcionar corretamente
			if (!player.move || player.position != side.Down) {
				//Seto o lado que o jogador esta movimentando
				player.position = side.Down;
				//Ativa os frames de movimentacao
				player.frame = frameSet.Down.frames;
				//Indica que o jogador está se movendo
				player.move = true;
			}
			//Move para baixo
			if (player.y + player.height + tamanhoMovimento > gameGeneral.height) {
				player.y = gameGeneral.height	 - player.height;
			} else {
				player.y += tamanhoMovimento;
			}
		} else {
			//Caso o jogador pare de apertar uma tecla direcional
			//verifica qual foi o ultimo lado
			//e deixa a sprite desse lado parado
			if (player.position == side.Down) {
				player.frame = 1;
			} else if (player.position == side.Up) {
				player.frame = 10;
			} else if (player.position == side.Right) {
				player.frame = 7;
			} else if (player.position == side.Left){
				player.frame = 4;
			}
			player.move = false;
		}
    },
});