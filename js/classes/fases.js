var Fases = {
	faseAbstract: function(arrayObjetos, musicFile) {
		var scene = new Scene();

		tempoLabel = new ScoreLabel(25,10);

		var arrayLen = arrayObjetos.length;
		for (i=0;i<arrayLen;i++) {
			scene.addChild(arrayObjetos[i]);
		}

		objetosGeneral.objetos = arrayObjetos;
		objetosGeneral.tamanho = arrayLen;

		player.resetPosition();

		scene.addChild(tempoLabel);
		scene.addChild(player);
		scene.backgroundColor = 'grey';

		MusicaJogo.playBackgroundMusic(musicFile);

		return scene;
	},
	fase1: function() {
	    faseAtual = 1;

		var objetos = [
			//Borda Esquerda
			new Objetos(imagens.carro1, 0, 0),
			new Objetos(imagens.carro2, 0, 103),
			new Objetos(imagens.carro3, 0, 213),
			new Objetos(imagens.carro4, 0, 324),
			new Objetos(imagens.carro7, 0, 420),
			new Objetos(imagens.carro2, 0, 515),
			//Borda Direita
			new Objetos(imagens.carro4, 550, 0),
			new Objetos(imagens.carro3, 550, 91),
			new Objetos(imagens.carro7, 550, 201),
			new Objetos(imagens.carro2, 550, 296),
			new Objetos(imagens.carro1, 550, 406),
			new Objetos(imagens.carro5, 550, 508),
			//Borda Superior
			new Objetos(imagens.carro1, 125, -25,90),
			new Objetos(imagens.carro5, 235, -25,90),
			new Objetos(imagens.carro2, 355, -30,90),
			new Objetos(imagens.carro3, 465, -30,90),
			//Borda inferior
			new Objetos(imagens.carro1, 75, 525,90),
			new Objetos(imagens.carro5, 185, 525,90),
			new Objetos(imagens.carro2, 355, 525,90),
			new Objetos(imagens.carro3, 465, 525,90),
			//Meio da fase 1
			new Objetos(imagens.carro5, 75, 80,90),
			new Objetos(imagens.carro2, 235, 75,90),
			new Objetos(imagens.carro4, 345, 80,90),
			new Objetos(imagens.carro1, 305, 130,90),
			new Objetos(imagens.carro7, 465, 180,90),
			new Objetos(imagens.carro6, 305, 180,90),
			new Objetos(imagens.carro5, 195, 180,90),
			new Objetos(imagens.carro7, 125, 230,90),
			new Objetos(imagens.carro6, 465, 280,90),
			new Objetos(imagens.carro7, 125, 280,90),
			new Objetos(imagens.carro5, 230, 280,90),
			new Objetos(imagens.carro4, 300, 330,90),
			new Objetos(imagens.carro7, 75, 380,90),
			new Objetos(imagens.carro2, 225, 375,90),
			new Objetos(imagens.carro2, 225, 425,90),
			new Objetos(imagens.carro1, 155, 480,90),
			new Objetos(imagens.carro1, 385, 380,90),
			new Objetos(imagens.carro3, 405, 475,90),
			new Objetos(imagens.cone, 448, 54,90),
			new Objetos(imagens.cone, 480, 54,90),
			new Objetos(imagens.cone, 515, 60,45),
			new Objetos(imagens.cone, 515, 100,90),
			new Objetos(imagens.cone, 132, 225,90),
			new Objetos(imagens.cone, 100, 225,90),
			new Objetos(imagens.buraco, 423, 247,90),

            //Familiares
            new Objetos(imagens.familia, 260, 565, 0, 4),
		];
		jogoIniciado = true;
		player = new Player();
		player.score = 300;

		return this.faseAbstract(objetos,musicas.musica2);	
	},
	fase2: function() {
	    faseAtual = 2;

        //Configuracao de destino ao passar pelo portal de origem
        destinoX = 170;
        destinoY = 480;
        origemX = 285;
        origemY = 225;

		var objetos = [
			//Borda Esquerda
			new Objetos(imagens.carro1, 0, 0),
			new Objetos(imagens.carro2, 0, 103),
			new Objetos(imagens.carro3, 0, 213),
			new Objetos(imagens.carro4, 0, 324),
			new Objetos(imagens.carro7, 0, 420),
			new Objetos(imagens.carro2, 0, 515),
			//Borda Direita
			new Objetos(imagens.carro4, 550, 0),
			new Objetos(imagens.carro3, 550, 91),
			new Objetos(imagens.carro7, 550, 201),
			new Objetos(imagens.carro2, 550, 296),
			new Objetos(imagens.carro1, 550, 406),
			new Objetos(imagens.carro5, 550, 508),
			//Borda Superior
			new Objetos(imagens.carro1, 125, -25,90),
			new Objetos(imagens.carro5, 235, -25,90),
			new Objetos(imagens.carro2, 355, -30,90),
			new Objetos(imagens.carro3, 465, -30,90),
			//Borda inferior
			new Objetos(imagens.carro1, 75, 525,90),
			new Objetos(imagens.carro5, 185, 525,90),
			new Objetos(imagens.carro2, 355, 525,90),
			new Objetos(imagens.carro3, 465, 525,90),
			//Meio da fase 2
			new Objetos(imagens.carro5, 60, 105),
			new Objetos(imagens.carro3, 110, 105),
			new Objetos(imagens.carro1, 160, 105),
			new Objetos(imagens.carro7, 210, 105),
			new Objetos(imagens.cone, 260, 105),
			new Objetos(imagens.cone, 300, 105),
			new Objetos(imagens.cone, 340, 105),
			new Objetos(imagens.cone, 340, 145),			
			new Objetos(imagens.carro6, 375, 105),
			new Objetos(imagens.carro6, 425, 105),
			new Objetos(imagens.carro4, 500, 280),
			new Objetos(imagens.carro1, 450, 280),
			new Objetos(imagens.carro2, 400, 280),
			new Objetos(imagens.carro3, 350, 280),
			new Objetos(imagens.carro5, 300, 280),
			new Objetos(imagens.carro6, 150, 280),
			new Objetos(imagens.carro1, 100, 280),
			new Objetos(imagens.carro5, 50, 280),
			new Objetos(imagens.cone, 240, 380),
			new Objetos(imagens.cone, 280, 380),

			new Objetos(imagens.carro1, 240, 400,90),
			new Objetos(imagens.carro5, 355, 400,90),
			new Objetos(imagens.carro6, 465, 400,90),

			new Objetos(imagens.cone, 50, 430),
			new Objetos(imagens.cone, 85, 430),
			new Objetos(imagens.cone, 120, 430),

			new Objetos(imagens.cone, 135, 462),
			new Objetos(imagens.cone, 135, 493),
			new Objetos(imagens.cone, 135, 524),

			new Objetos(imagens.cone, 180, 515),
			new Objetos(imagens.cone, 215, 515),

			//Armadilhas
			new Objetos(imagens.buraco, 245, 297),

			//Portal Entrada
			new Objetos(imagens.buraco, 270, 147, 0, 2),

			//Portal Saida
			new Objetos(imagens.buraco, 70, 470, 0, 3),

            //Familia do jogador
            new Objetos(imagens.familia, 260, 565, 0, 4),

		];

		//Atualiza score
        player.score += scoreTotal;
		return this.faseAbstract(objetos,musicas.musica2);	
	},
}
