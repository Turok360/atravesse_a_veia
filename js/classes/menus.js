enchant();
var Menu = {
	gameOver: function() {
		var scene = this.gameOverMenu();
		jogoIniciado = false;
		gameGeneral.replaceScene(scene);
	},
	gameOverMenu: function() {
		//Gera uma nova cena
		var scene = new Scene();

		scene.addChild(new Objetos(imagens.gameover, 0, 0),);
		//Gera o label do Game Over
		var label = new Label("Game Over");
		label.x = 25;
		label.y = 30;
		label.font = '25px serif';
		label.color = 'Black';

		scene.backgroundColor = 'Black';
			
		scene.addChild(label);

		//Botão para iniciar o jogo
		var btnReiniciar = new Button();
		btnReiniciar.text = 'Reiniciar';
		btnReiniciar.x = 35;
		btnReiniciar.y = 110;
		scene.addChild(btnReiniciar);


	    btnReiniciar.addEventListener("touchstart", function() {
			var jogo = Fases.fase1();
	       	gameGeneral.replaceScene(jogo);
	    });

	    //Botão para iniciar o jogo
		var btnIniciar = new Button();
		btnIniciar.text = 'Menu Inical';
		btnIniciar.x = 35;
		btnIniciar.y = 70;
		scene.addChild(btnIniciar);


	    btnIniciar.addEventListener("touchstart", function() {
			var jogo = Menu.inicial();
	       	gameGeneral.replaceScene(jogo);
	    });

		MusicaJogo.playBackgroundMusic(musicas.gameover)

	    return scene;

	},
    gameWin: function() {
        var scene = this.gameWinMenu();
        jogoIniciado = false;
        gameGeneral.replaceScene(scene);
    },
    gameWinMenu: function() {
        //Gera uma nova cena
        var scene = new Scene();

        scene.addChild(new Objetos(imagens.gameover, 0, 0),);
        //Gera o label do Game Over
        var label = new Label("Voce Ganhou!");
        label.x = 25;
        label.y = 30;
        label.font = '25px serif';
        label.color = 'Black';

        //Gera o label da pontuacao
        var scoreLabel = new Label("Score: " + scoreTotal);
        scoreLabel.x = 25;
        scoreLabel.y = 60;
        scoreLabel.font = '25px serif';
        scoreLabel.color = 'Black';

        scene.backgroundColor = 'Black';

        scene.addChild(label);
        scene.addChild(scoreLabel);

        //Botão para iniciar o jogo
        var btnReiniciar = new Button();
        btnReiniciar.text = 'Reiniciar';
        btnReiniciar.x = 35;
        btnReiniciar.y = 210;
        scene.addChild(btnReiniciar);


        btnReiniciar.addEventListener("touchstart", function() {
            var jogo = Fases.fase1();
            gameGeneral.replaceScene(jogo);
        });

        //Botão para iniciar o jogo
        var btnIniciar = new Button();
        btnIniciar.text = 'Menu Inical';
        btnIniciar.x = 35;
        btnIniciar.y = 170;
        scene.addChild(btnIniciar);


        btnIniciar.addEventListener("touchstart", function() {
            var jogo = Menu.inicial();
            gameGeneral.replaceScene(jogo);
        });

        MusicaJogo.playBackgroundMusic(musicas.gameover)

        return scene;

    },
	inicial: function() {
		//Gera uma nova cena
		var scene = new Scene();

		scene.addChild(new Objetos(imagens.capa, 0, 0),);
		var label = new Label("#AjudeOCego");
		label.x = 20;
		label.y = 20;
		label.font = '25px serif';
		label.color = 'Black';
		scene.addChild(label);

		//Botão para iniciar o jogo
		var btnIniciar = new Button();
		btnIniciar.text = 'Iniciar Jogo';
		btnIniciar.x = 450;
		btnIniciar.y = 50;
		scene.addChild(btnIniciar);


	    btnIniciar.addEventListener("touchstart", function() {
			var jogo = Fases.fase1();
	       	gameGeneral.replaceScene(jogo);
	    });

		//Botão para exibir os records
		var btnRecords = new Button();
		btnRecords.text = 'Records';
		btnRecords.x = 450;
		btnRecords.y = 80;
		scene.addChild(btnRecords);

        btnRecords.addEventListener("touchstart", function() {
            var jogo = Menu.records();
            gameGeneral.replaceScene(jogo);
        });

		//usa o localStorage para armazenar se o som fica habilitado ou  não
		var habilitarSom = window.localStorage.getItem('soundGame');
		var texto = 'Desabilitar Som';

		if (habilitarSom == false) {
			texto = 'Habilitar Som';
		}

		//Botão para habilitar/desabilitar o som do jogo
		var btnSom = new Button();
		btnSom.text = texto;
		btnSom.x = 450;
		btnSom.y = 110;
		scene.addChild(btnSom);


	    btnSom.addEventListener("touchstart", function() {
			//usa o localStorage para armazenar se o som fica habilitado ou  não
			var habilitarSom = window.localStorage.getItem('soundGame');
			var texto = '';

			if (habilitarSom == false) {
			    texto = 'Desabilitar Som';
				window.localStorage.setItem('soundGame', 1);
			} else {
				texto = 'Habilitar Som';
				window.localStorage.setItem('soundGame', 0);
			}

			btnSom.text = texto;	
	    });

		MusicaJogo.playBackgroundMusic(musicas.inicial);

		return scene;
		
	},
    records: function() {
        //Gera uma nova cena
        var scene = new Scene();

        scene.addChild(new Objetos(imagens.capa, 0, 0),);
        var label = new Label("#AjudeOCego - Records");
        label.x = 20;
        label.y = 20;
        label.font = '25px serif';
        label.color = 'Black';
        scene.addChild(label);

        //Botão para iniciar o jogo
        var btnVoltar = new Button();
        btnVoltar.text = 'Voltar';
        btnVoltar.x = 450;
        btnVoltar.y = 50;
        scene.addChild(btnVoltar);

        btnVoltar.addEventListener("touchstart", function() {
            var jogo = Menu.inicial();
            gameGeneral.replaceScene(jogo);
        });

        var base = 60;
        var incremento = 20;
    	var yRecord = 0;
		        
        for (var i in records) {
        	yRecord = base + (incremento*i)
        	recordsLabel = new MutableText(10, yRecord);
        	var idx = parseInt(i) + 1;
        	if (idx < 10) {
        		idx = '0' + idx.toString();
        	}
        	var nome = records[i].nome;
            if (nome.length > 3) {
            	nome = nome.substring(0,3);
            }	
            recordsLabel.text = idx + ") " + nome + " - " + records[i].pontuacao;
        	
        	scene.addChild(recordsLabel);
        }

        return scene;
    },
}


var MusicaJogo = {
	//Método responsável por tocar os sons do jogo
	playSound: function(soundFilePath) {
		if (gameGeneral.dripSound) {
			//Para a música anterior
			gameGeneral.dripSound.stop();
		}
		//Inicializa a musica
		gameGeneral.dripSound = gameGeneral.assets[soundFilePath];
		//Toca a musica
		gameGeneral.dripSound.play();
	},
	//Método responsável por tocar as músicas de fundo do jogo
	playBackgroundMusic: function(musicFilePath) {
		if (gameGeneral.bgm) {
			//Para a música anterior
			gameGeneral.bgm.stop();	
		}
		//Inicializa a musica
		gameGeneral.bgm = gameGeneral.assets[musicFilePath];
		//Toca a musica
		gameGeneral.bgm.play();

	}

}
