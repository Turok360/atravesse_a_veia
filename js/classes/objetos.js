
/*Classe dos objetos que formam o labirinto
 * Tipos de objetos:
 * 1 = Obstaculo (padrao)
 * 2 = Portal de Origem
 * 3 = Portal de Destino
 * 4 = Chegada
*/
var Objetos = enchant.Class.create(enchant.Sprite, {
	initialize: function(definicaoObjeto, x, y, rotation = 0, tipo = 1) {
		enchant.Sprite.call(this,definicaoObjeto.largura, definicaoObjeto.altura);

		this.image = gameGeneral.assets[definicaoObjeto.arquivo];
		this.x = x;
		this.y = y;
		this.tipo = tipo;
		this.rotate(rotation);
		this.scale(definicaoObjeto.escala, definicaoObjeto.escala);
	}
});