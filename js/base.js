enchant(); //Inicializa lib enchant.js

window.onload = function() {
	$( "#iniciarJogo" ).click(function() {
		//Esconder div de inserir dados
		$( "#index" ).hide();
		//Core do enchant
		gameGeneral = new Core(600,600);
		 

		//Configuracoes
		gameGeneral.scale = 1;
		gameGeneral.fps = 30;


	    gameGeneral.preload(preload);

		//Inicia loop
		gameGeneral.onload = function(){
			//Game Loop
			gameGeneral.addEventListener('enterframe', function(e) {
				if (jogoIniciado) {
					//Atualiza o tempo
					var tempo = parseInt(tempoLabel.age / 10);
					scoreTotal = player.score - tempo;
					tempoLabel.score = scoreTotal;

					if (scoreTotal <= 0) {
						Menu.gameOver();
					}

					var objetos = objetosGeneral.objetos;
					var qtdeObjetos = objetosGeneral.tamanho;
					//Verifica se o jogador bateu em algum carro
					for (i=0;i<qtdeObjetos;i++) {
						//Verifica se o jogador está encostando no obstaculo
						if (objetos[i].intersectStrict(player) ) {
							if(objetos[i].tipo == 1) {
								Records.salvar(player.nome,scoreTotal);
	                            Menu.gameOver();
							} else if (objetos[i].tipo == 2) {
								player.x = destinoX;
								player.y = destinoY;
								console.log("Teleporte para portal Destino posX: " + destinoX + " posY: " + destinoY);
							} else if (objetos[i].tipo == 3) {
	                            player.x = origemX;
	                            player.y = origemY;
	                            console.log("Teleporte para portal Origem posX: " + origemX + " posY: " + origemY);
	                        } else if (objetos[i].tipo == 4) {
								if(faseAtual == 1){
	                                console.log("Passou de fase!");
	                                
	                                //Cahama fase 2
	                                var jogo = Fases.fase2();
	                                gameGeneral.replaceScene(jogo);
								}else if(faseAtual == 2) {
									Records.salvar(player.nome,scoreTotal);
	                                Menu.gameWin();
	                                console.log("Fim de Jogo! You Win!");
	                            }
	                        }
						}

					}
				}

				//Loop na musica de fundo
				//A biblioteca enchant js não possui suporte a loop de som por padrão, logo é necessário
				//implementar de forma manual
	            if (gameGeneral.bgm && gameGeneral.bgm.currentTime >= gameGeneral.bgm.duration) {
	                gameGeneral.bgm.play();
	            }

	            //Ativa/inativa o som do jogo
	           	if (gameGeneral.bgm) {
	           		gameGeneral.bgm.volume = window.localStorage.getItem('soundGame');
	           	}
	            //Ativa/inativa o som do jogo
	           	if (gameGeneral.dripSound) {
	           		gameGeneral.bgm.volume = window.localStorage.getItem('soundGame');
	           	}

			});

			//Inicializa o jogador
			var gameScene = Menu.inicial();
	        gameScene.backgroundColor = 'grey';
	        gameGeneral.replaceScene(gameScene);
		}

		//Verifica não existe a variavel do som, se não houver, seta como ativo
		var som = window.localStorage.getItem('soundGame');
		if (som == null) 
			window.localStorage.setItem('soundGame', 1);

		gameGeneral.start();
	});
};

	
