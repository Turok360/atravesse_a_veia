
var side = {
	Down: 2,
	Up: 3,
	Right: 0,
	Left:1
};

//Framet com os frames do jogador
var frameSet = {
	Up: {
		frames: [10,9,10,11]
	},
	Down: {
		frames: [1,0,1,2]
	},
	Left: {
		frames: [4,3,4,5]
	},
	Right: {
		frames: [7,6,7,8]
	},
};

//Definição das imagens do jogo
var imagens = {
	jogador: {
		arquivo: 'imagens/Player.png',
		largura: 31.6,
		altura: 32,
		escala: 1
	},
    familia: {
        arquivo: 'imagens/Familia.png',
        largura: 63.6,
        altura: 34,
        escala: 1
    },
	carro1: {
		arquivo: 'imagens/vehicles/1.png',
		largura: 50,
		altura: 102,
		escala: 1
	},
	carro2: {
		arquivo: 'imagens/vehicles/2.png',
		largura: 50,
		altura: 110,
		escala: 1
	},
	carro3: {
		arquivo: 'imagens/vehicles/3.png',
		largura: 50,
		altura: 110,
		escala: 1
	},
	carro4: {
		arquivo: 'imagens/vehicles/4.png',
		largura: 50,
		altura: 91,
		escala: 1
	},
	carro5: {
		arquivo: 'imagens/vehicles/5.png',
		largura: 50,
		altura: 101,
		escala: 1
	},
	carro6: {
		arquivo: 'imagens/vehicles/6.png',
		largura: 50,
		altura: 102,
		escala: 1
	},
	carro7: {
		arquivo: 'imagens/vehicles/7.png',
		largura: 50,
		altura: 95,
		escala: 1
	},
	cone: {
		arquivo: 'imagens/cone.png',
		largura: 30,
		altura: 28,
		escala: 1
	},
	buraco: {
		arquivo: 'imagens/Buraco.png',
		largura: 60,
		altura: 65,
		escala: 1
	},
	capa: {
		arquivo: 'imagens/capa.jpg',
		largura: 600,
		altura: 600,
		escala: 1
	},
	gameover: {
		arquivo: 'imagens/gameover.jpg',
		largura: 600,
		altura: 600,
		escala: 1
	}	
};

var musicas = {
	musica1: 'musicas/music1.mp3',
	musica2: 'musicas/music2.mp3',
	inicial: 'musicas/inicial.wav',
	gameover: 'musicas/gameover.wav',
};

var gameGeneral;
var tempoLabel;

var player;

//Faz o preload das imagens
var preload = [
	imagens.jogador.arquivo,
    imagens.familia.arquivo,
	imagens.carro1.arquivo,
	imagens.carro2.arquivo,
	imagens.carro3.arquivo,
	imagens.carro4.arquivo,
	imagens.carro5.arquivo,
	imagens.carro6.arquivo,
	imagens.carro7.arquivo,
	imagens.cone.arquivo,
	imagens.buraco.arquivo,
	imagens.capa.arquivo,
	imagens.gameover.arquivo,
	musicas.musica1,
	musicas.musica2,
	musicas.inicial,
	musicas.gameover,
];

var tamanhoMovimento = 3;
var jogoIniciado = false;
var faseAtual = 1;
var destinoX = 0;
var destinoY = 0;
var origemX = 0;
var origemY = 0;
var objetosGeneral = {};
var scoreTotal = 0;

//Carrega array de records
var records = JSON.parse(window.localStorage.getItem('Records'));